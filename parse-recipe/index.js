var request = require('request'),
  cheerio = require('cheerio'),
  mongoClient = require("mongodb").MongoClient;

let headers = {
  "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
  "Accept-Language": "cs,en-us;q=0.7,en;q=0.3",
  "Connection": "keep-alive",
  "User-Agent": "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:24.0) Gecko/20100101 Firefox/30.0"
};
module.exports = function (context, urlToParse) {
  context.log("start: " + urlToParse);
  // urlToParse = 'http://www.recepty.cz/recept/dusena-kari-dyne-v-mletem-mase-157423';

  request({uri: urlToParse, gzip: true, headers: headers}, (error, response, body) => {
      if (!error && response.statusCode == 200) {
        let ingredients = [], name = '', description = '', image;
        var $ = cheerio.load(body, {xmlMode: true});
        $('h1.fn').each(function (idx) {
          name = $(this).text();
        });
        $('td.ingredient-list-title').each(function (idx) {
          let tt = $(this).text().replace(/\s/g, " ").trim().split('  ')[0].trim();
          ingredients[idx] = {name: tt}
        });
        $('td.ingredient-list-quantity').each(function (idx) {
          let tt = $(this).text().replace(/\s/g, " ").replace(/\s/g, " ").trim().split('  ')[0].trim();
          ingredients[idx].quantity = tt.split('&nbsp;')[0];
          ingredients[idx].units = tt.split('&nbsp;')[1]
        });
        $('div.wikiPreview p').each(function (idx) {
          description += $(this).text() + '\n';
        });
        $('div#recipe-detail-image > a.photo-gallery img.photo').each(function (idx) {
          image = $(this).attr("src");
        });

        mongoClient.connect("mongodb://chcijist:yggUJORjmNT4cTnuIIFjZKELXnL5knQfj0Cs0IZ0lFGvbaycn4Wbqcyjjk3sbD7opTntJxGZ2KptNKAmaazsiQ==@chcijist.documents.azure.com:10250/products?ssl=true", function (err, db) {
          var collection = db.collection('recipes2');
          collection.insertOne({name: name, ingredients: ingredients, image: image, description: description, id: new Date().getTime() + '' + Math.round(Math.random() * 10000)}, (err, resp) => {
            "use strict";
            context.log('saved' + name);
            context.done();
          });
        })
      } else {
        context.log(error);
        context.res = {
          status: 400,
          body: "error"
        };
      }

    }
  );
};