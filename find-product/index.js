var request = require('request'),
  mongo = require("mongodb"),
  removeDiacritics = require('diacritics').remove,
  mongoClient = mongo.MongoClient;

module.exports = function (context, req) {
  context.log("start");
  if (req.query.name) {
    request({
        uri: 'https://chcijist.search.windows.net/indexes/temp-test/docs?api-version=2015-02-28&search=' + removeDiacritics(req.query.name),
        headers: {'api-key': '6E3BC3C463C838770B11B7C136084B8C', 'Content-Type': 'application/json'}
      },
      (err, res, body)=> {
        "use strict";
        body = JSON.parse(body);
        if (!body.value || body.value.length === 0) {
          context.log(body);
          context.res = {
            status: 200,
            body: []
          };
          context.done();
        } else {
          var ids = body.value.splice(0, 3).map((id) => {
            return mongo.ObjectId(id.id);
          });
          mongoClient.connect("mongodb://chcijist:yggUJORjmNT4cTnuIIFjZKELXnL5knQfj0Cs0IZ0lFGvbaycn4Wbqcyjjk3sbD7opTntJxGZ2KptNKAmaazsiQ==@chcijist.documents.azure.com:10250/products?ssl=true", function (err, db) {
            context.log('connected');
            var collection = db.collection('products');
            // collection.createIndex({"name":"text"})
            context.log('use collection');

            collection.find({"_id": {$in: ids}}).toArray(function (err, docs) {
              if (err) {
                context.res = {
                  status: 400,
                  body: "sdsPlease pass a name on the query string or in the request body"
                };
                context.log(err);
              } else {
                let resp = [];
                docs.forEach((item)=>{
                  ids.forEach((id ,key) =>{
                    if(id.id.toString('ascii') == item._id.id.toString('ascii')) {
                      resp[key] = item;
                    }
                  })

                });
                context.log("Found the following records");
                context.log(docs);
                context.res = {

                  status: 200, /* Defaults to 200 */
                  body: resp
                };
              }
              db.close();
              context.done();
            });
            if (err) {
              context.log(err);
              context.res = {
                status: 400,
                body: "sdsPlease pass a name on the query string or in the request body"
              };
              db.close();
              context.done();
            }

          });
        }

      })


  }
  else {
    context.res = {
      status: 400,
      body: "sdsPlease pass a name on the query string or in the request body"
    };
    context.done();
  }

};