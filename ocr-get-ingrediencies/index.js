var request = require('request'),
  removeDiacritics = require('diacritics').remove;

let headers = {
  "Content-Type": "application/json",
  "Host": "api.projectoxford.ai",
  "Ocp-Apim-Subscription-Key": "9746f907da944422b76c506692b62327"
};
var numCallbacks = 0, res =[];
module.exports = function (context, req) {
  context.log("start: " + req.query.url);

  request({
      method: 'POST',
      uri: 'https://api.projectoxford.ai/vision/v1.0/ocr?language=cs&detectOrientation=true',
      headers: headers,
      json: {url: req.query.url}
    }, (error, response, body) => {
      if (!error && response.statusCode == 200) {

        body.regions.forEach((collection) => {
          "use strict";
          let reg = new RegExp(/^([a-z]|\s)+$/g)
          let newCollection = [];
          collection.lines.forEach((line) => {
            let words = line.words.filter((word) => {
              word.text = String(removeDiacritics(String(word.text)));
              return (word.text.length > 2 && word.text != Number(word.text) && reg.test(word.text))
            });
            if(words.length) {
              newCollection.push(words);
            }
          });
          let strings = newCollection.map((line) => {
            return line.map((word) => {
              return word.text;
            }).join(" ");
          });
          strings.forEach((string)=>{
            if(string && string.length>1) {
              numCallbacks++;
              request({
                method: 'POST',
                uri: 'https://chcijist-api.azurewebsites.net/api/find-product?code=zPViHSkM4aUo2GoMjf1UyuORAmarW8LEbxR2/ydQEaK54am6Z92qcw==&name=' + string,
                headers: {'Content-Type': 'application/json'}
              }, (err, res, body) => {
                sendResponse(context, body, string);
              })
            }

          })

        });
      } else {
        context.log(error);
        context.res = {
          status: 400,
          body: "error"
        };
        context.done()
      }

    }
  );
};

function sendResponse(context, resp, string) {
  --numCallbacks;
  if(resp[0] === '[') {
    res.push(JSON.parse(resp));
  } else {
    context.log(string);
    context.log(resp);
  }

  if(numCallbacks === 0) {
    context.res = {
      status: 200,
      body: res
    };
    context.done()
  }

}